// Initialize the close buttons for existing list items
var myNodelist = document.getElementsByTagName("LI");
for (var i = 0; i < myNodelist.length; i++) {
    addCloseButton(myNodelist[i]);
}

// Click on a close button to hide the current list item
function addCloseButton(item) {
    var span = document.createElement("SPAN");
    var txt = document.createTextNode("\u00D7");
    span.className = "close";
    span.appendChild(txt);
    item.appendChild(span);

    span.onclick = function() {
        var div = this.parentElement;
        div.style.display = "none";
    }
}

// Add a "checked" symbol when clicking on a list item
var list = document.querySelector('ul');
list.addEventListener('click', function(ev) {
    if (ev.target.tagName === 'LI') {
        ev.target.classList.toggle('checked');
    }
}, false);

// Create a new list item when clicking on the "Add" button
function newElement() {
    var inputValue = document.getElementById("myInput").value;
    var category = document.getElementById("category").value;

    if (inputValue === '' || category === '') {
        alert("Both task text and category are required!");
        return;
    }

    var li = document.createElement("li");
    var taskText = document.createTextNode(inputValue);
    var categoryText = document.createTextNode(" [" + category + "] ");
    li.appendChild(taskText);
    li.appendChild(categoryText);

    document.getElementById("myUL").appendChild(li);
    document.getElementById("myInput").value = "";

    // Add close button to the new item
    addCloseButton(li);
}
